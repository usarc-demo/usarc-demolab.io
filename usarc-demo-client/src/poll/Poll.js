import React, {Component} from 'react';
import './Poll.css';
import {Avatar, Button, notification, Row} from 'antd';
import {Link} from 'react-router-dom';
import {getAvatarColor} from '../util/Colors';
import {formatDateTime} from '../util/Helpers';
import {updatePoll} from "../util/APIUtils";

// const RadioGroup = Radio.Group;

class Poll extends Component {
    calculatePercentage = (choice) => {
        if (this.props.poll.totalVotes === 0) {
            return 0;
        }
        return (choice.voteCount * 100) / (this.props.poll.totalVotes);
    };

    isSelected = (choice) => {
        return this.props.poll.selectedChoice === choice.id;
    }

    getWinningChoice = () => {
        return this.props.poll.choices.reduce((prevChoice, currentChoice) =>
                currentChoice.voteCount > prevChoice.voteCount ? currentChoice : prevChoice,
            {voteCount: -Infinity}
        );
    }

    onReject(poll) {
        const newData = Object.assign({}, poll)
        newData.jsonData.rejected = true
        newData.jsonData.approved = false
        this.updatePoll(poll)
    }

    onApprove(poll) {
        const newData = Object.assign({}, poll)
        newData.jsonData.rejected = false
        newData.jsonData.approved = true
        this.updatePoll(poll)
    }

    updatePoll(newData) {
        console.log(newData)
        updatePoll(newData)
            .then(response => {
                this.props.history.push("/");
                notification.success({
                    message: 'USAR Soldier Response',
                    description: 'Suggestion is approved successfully'
                });
            }).catch(error => {
            if(error.status === 401) {
                this.props.handleLogout('/login', 'error', 'You have been logged out. Please login create poll.');
            } else {
                console.log(error)
                notification.error({
                    message: 'USAR Soldier Response',
                    description: error.message || 'Sorry! Something went wrong. Please try again!'
                });
            }
        });
    }

    getTimeRemaining = (poll) => {
        const expirationTime = new Date(poll.expirationDateTime).getTime();
        const currentTime = new Date().getTime();

        var difference_ms = expirationTime - currentTime;
        var seconds = Math.floor((difference_ms / 1000) % 60);
        var minutes = Math.floor((difference_ms / 1000 / 60) % 60);
        var hours = Math.floor((difference_ms / (1000 * 60 * 60)) % 24);
        var days = Math.floor(difference_ms / (1000 * 60 * 60 * 24));

        let timeRemaining;

        if (days > 0) {
            timeRemaining = days + " days left";
        } else if (hours > 0) {
            timeRemaining = hours + " hours left";
        } else if (minutes > 0) {
            timeRemaining = minutes + " minutes left";
        } else if (seconds > 0) {
            timeRemaining = seconds + " seconds left";
        } else {
            timeRemaining = "less than a second left";
        }

        return timeRemaining;
    }

    render() {
        // const pollChoices = [];
        // if (this.props.poll.selectedChoice || this.props.poll.expired) {
        //     const winningChoice = this.props.poll.expired ? this.getWinningChoice() : null;
        //
        //     this.props.poll.choices.forEach(choice => {
        //         pollChoices.push(<CompletedOrVotedPollChoice
        //             key={choice.id}
        //             choice={choice}
        //             isWinner={winningChoice && choice.id === winningChoice.id}
        //             isSelected={this.isSelected(choice)}
        //             percentVote={this.calculatePercentage(choice)}
        //         />);
        //     });
        // } else {
        //     this.props.poll.choices.forEach(choice => {
        //         pollChoices.push(<Radio className="poll-choice-radio" key={choice.id}
        //                                 value={choice.id}>{choice.text}</Radio>)
        //     })
        // }
        return (
            <div className="poll-content">
                <div className="poll-header">
                    <div className="poll-creator-info">
                        <Link className="creator-link" to={`/users/${this.props.poll.createdBy.username}`}>
                            <Avatar className="poll-creator-avatar"
                                    style={{backgroundColor: getAvatarColor(this.props.poll.createdBy.name)}}>
                                {this.props.poll.createdBy.name[0].toUpperCase()}
                            </Avatar>
                            <span className="poll-creator-name">
                                {this.props.poll.createdBy.name}
                            </span>
                            <span className="poll-creator-username">
                                @{this.props.poll.createdBy.username}
                            </span>
                            <span className="poll-creation-date">
                                {formatDateTime(this.props.poll.creationDateTime)}
                            </span>
                        </Link>
                    </div>
                    {/*<div className="poll-question">*/}
                    {/*    {this.props.poll.question}*/}
                    {/*</div>*/}
                </div>
                <Row className="poll-form-row">
                    <div className="poll-question">
                        Orders
                    </div>
                    <div>
                        {this.props.poll.jsonData.orders.text ? this.props.poll.jsonData.orders.text : this.props.poll.jsonData.orders}
                    </div>
                </Row>
                <Row className="poll-form-row">
                    <div className="poll-question">
                        Promotions, Awards or Evaluations
                    </div>
                    <div>
                        {this.props.poll.jsonData.promo_awards_eval.text ? this.props.poll.jsonData.promo_awards_eval.text : this.props.poll.jsonData.promo_awards_eval}
                    </div>
                </Row>
                <Row className="poll-form-row">
                    <div className="poll-question">
                        Logistics
                    </div>
                    <div>
                        {this.props.poll.jsonData.logistics.text ? this.props.poll.jsonData.logistics.text : this.props.poll.jsonData.logistics}
                    </div>
                </Row>
                <Row className="poll-form-row">
                    <div className="poll-question">
                        Services
                    </div>
                    <div>
                        {this.props.poll.jsonData.services.text ? this.props.poll.jsonData.services.text : this.props.poll.jsonData.services}
                    </div>
                </Row>
                <Row className="poll-form-row">
                    <div className="poll-question">
                        USAR System/Website Access
                    </div>
                    <div>
                        {this.props.poll.jsonData.system_website_access.text ? this.props.poll.jsonData.system_website_access.text : this.props.poll.jsonData.system_website_access}
                    </div>
                </Row>
                <Row className="poll-form-row">
                    <div className="poll-question">
                        Suggestion
                    </div>
                    <div>
                        {this.props.poll.jsonData.other.text ? this.props.poll.jsonData.other.text : this.props.poll.jsonData.other}
                    </div>
                </Row>
                {this.props.toApprove ? <Row className="poll-form-row">
                    <Button type="danger"
                            size="large"
                            style={{ width: '100%'}}
                            onClick={() => this.onReject(this.props.poll)}
                    >Reject</Button>
                    <br /><br />
                    <Button type="primary"
                            size="large"
                            style={{  width: '100%'}}
                            onClick={() => this.onApprove(this.props.poll)}
                    >Approve</Button>
                </Row> : null}
                {/*<div className="poll-choices">*/}
                {/*    <RadioGroup */}
                {/*        className="poll-choice-radio-group" */}
                {/*        onChange={this.props.handleVoteChange} */}
                {/*        value={this.props.currentVote}>*/}
                {/*        { pollChoices }*/}
                {/*    </RadioGroup>*/}
                {/*</div>*/}
                {/*<div className="poll-footer">*/}
                {/*    { */}
                {/*        !(this.props.poll.selectedChoice || this.props.poll.expired) ?*/}
                {/*        (<Button className="vote-button" disabled={!this.props.currentVote} onClick={this.props.handleVoteSubmit}>Vote</Button>) : null */}
                {/*    }*/}
                {/*    <span className="total-votes">{this.props.poll.totalVotes} votes</span>*/}
                {/*    <span className="separator">•</span>*/}
                {/*    <span className="time-left">*/}
                {/*        {*/}
                {/*            this.props.poll.expired ? "Final results" :*/}
                {/*            this.getTimeRemaining(this.props.poll)*/}
                {/*        }*/}
                {/*    </span>*/}
                {/*</div>*/}
            </div>
        );
    }
}

// function CompletedOrVotedPollChoice(props) {
//     return (
//         <div className="cv-poll-choice">
//             <span className="cv-poll-choice-details">
//                 <span className="cv-choice-percentage">
//                     {Math.round(props.percentVote * 100) / 100}%
//                 </span>
//                 <span className="cv-choice-text">
//                     {props.choice.text}
//                 </span>
//                 {
//                     props.isSelected ? (
//                         <Icon
//                             className="selected-choice-icon"
//                             type="check-circle-o"
//                         />) : null
//                 }
//             </span>
//             <span className={props.isWinner ? 'cv-choice-percent-chart winner' : 'cv-choice-percent-chart'}
//                   style={{width: props.percentVote + '%'}}>
//             </span>
//         </div>
//     );
// }


export default Poll;
