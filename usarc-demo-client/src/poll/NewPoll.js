import React, {Component} from 'react';
import {createPoll} from '../util/APIUtils';
import {POLL_CHOICE_MAX_LENGTH, POLL_QUESTION_MAX_LENGTH} from '../constants';
import './NewPoll.css';
import {Button, Col, Form, Icon, Input, notification, Select} from 'antd';

const Option = Select.Option;
const FormItem = Form.Item;
const { TextArea } = Input

class NewPoll extends Component {
    constructor(props) {
        super(props);
        this.state = {
            question: {
                text: 'dummy'
            },
            choices: [{
                text: 'dummy'
            }, {
                text: 'dummy'
            }],
            pollLength: {
                days: 6,
                hours: 0
            },
            orders: {
                text: ''
            },
            promo_awards_eval: {
                text: ''
            },
            logistics: {
                text: ''
            },
            services: {
                text: ''
            },
            system_website_access: {
                text: ''
            },
            other: {
                text: ''
            }
        };
        this.addChoice = this.addChoice.bind(this);
        this.removeChoice = this.removeChoice.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleQuestionChange = this.handleQuestionChange.bind(this);
        this.handleChoiceChange = this.handleChoiceChange.bind(this);
        this.handlePollDaysChange = this.handlePollDaysChange.bind(this);
        this.handlePollHoursChange = this.handlePollHoursChange.bind(this);
        this.isFormInvalid = this.isFormInvalid.bind(this);
    }

    addChoice(event) {
        const choices = this.state.choices.slice();
        this.setState({
            choices: choices.concat([{
                text: ''
            }])
        });
    }

    removeChoice(choiceNumber) {
        const choices = this.state.choices.slice();
        this.setState({
            choices: [...choices.slice(0, choiceNumber), ...choices.slice(choiceNumber+1)]
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        const pollData = {
            question: this.state.question.text,
            choices: this.state.choices.map(choice => {
                return {text: choice.text}
            }),
            pollLength: this.state.pollLength,
            jsonData: {
                orders: this.state.orders.text,
                promo_awards_eval: this.state.promo_awards_eval.text,
                logistics: this.state.logistics.text,
                services: this.state.services.text,
                system_website_access: this.state.system_website_access.text,
                other: this.state.other.text
            }
        };

        createPoll(pollData)
        .then(response => {
            this.props.history.push("/");
        }).catch(error => {
            if(error.status === 401) {
                this.props.handleLogout('/login', 'error', 'You have been logged out. Please login create poll.');
            } else {
                notification.error({
                    message: 'USAR Soldier Response',
                    description: error.message || 'Sorry! Something went wrong. Please try again!'
                });
            }
        });
    }

    validateQuestion = (questionText) => {
        if(questionText.length === 0) {
            return {
                validateStatus: 'error',
                errorMsg: 'Please enter your question!'
            }
        } else if (questionText.length > POLL_QUESTION_MAX_LENGTH) {
            return {
                validateStatus: 'error',
                errorMsg: `Question is too long (Maximum ${POLL_QUESTION_MAX_LENGTH} characters allowed)`
            }
        } else {
            return {
                validateStatus: 'success',
                errorMsg: null
            }
        }
    }

    handleQuestionChange(event) {
        const value = event.target.value;
        this.setState({
            question: {
                text: value,
                ...this.validateQuestion(value)
            }
        });
    }

    validateChoice = (choiceText) => {
        if(choiceText.length === 0) {
            return {
                validateStatus: 'error',
                errorMsg: 'Please enter a choice!'
            }
        } else if (choiceText.length > POLL_CHOICE_MAX_LENGTH) {
            return {
                validateStatus: 'error',
                errorMsg: `Choice is too long (Maximum ${POLL_CHOICE_MAX_LENGTH} characters allowed)`
            }
        } else {
            return {
                validateStatus: 'success',
                errorMsg: null
            }
        }
    }

    handleChoiceChange(event, index) {
        const choices = this.state.choices.slice();
        const value = event.target.value;

        choices[index] = {
            text: value,
            ...this.validateChoice(value)
        }

        this.setState({
            choices: choices
        });
    }

    handleDefaultFieldChange(key, value) {
        const data = Object.assign(this.state[key], {text: value, ...this.validateQuestion(value)});
        this.setState({
            [key]: data
        });
    }

    handlePollDaysChange(value) {
        const pollLength = Object.assign(this.state.pollLength, {days: value});
        this.setState({
            pollLength: pollLength
        });
    }

    handlePollHoursChange(value) {
        const pollLength = Object.assign(this.state.pollLength, {hours: value});
        this.setState({
            pollLength: pollLength
        });
    }

    isFormInvalid() {
        if(
            this.state.orders.validateStatus !== 'success' ||
            this.state.promo_awards_eval.validateStatus !== 'success' ||
            this.state.logistics.validateStatus !== 'success' ||
            this.state.services.validateStatus !== 'success' ||
            this.state.system_website_access.validateStatus !== 'success'
        ) {
            return true;
        }

        // for(let i = 0; i < this.state.choices.length; i++) {
        //     const choice = this.state.choices[i];
        //     if(choice.validateStatus !== 'success') {
        //         return true;
        //     }
        // }
    }

    render() {
        const choiceViews = [];
        this.state.choices.forEach((choice, index) => {
            choiceViews.push(<PollChoice key={index} choice={choice} choiceNumber={index} removeChoice={this.removeChoice} handleChoiceChange={this.handleChoiceChange}/>);
        });

        return (
            <div className="new-poll-container">
                <h1 className="page-title">Your Suggestion</h1>
                <div className="new-poll-content">
                    <Form onSubmit={this.handleSubmit} className="create-poll-form">
                        <FormItem validateStatus={this.state.orders.validateStatus}
                                  help={this.state.orders.errorMsg} className="poll-form-row">
                            <Col span={24}>
                                Orders*
                            </Col>
                            <Col span={24}>
                                <span style = {{ marginRight: '18px' }}>
                                    <Select
                                        onChange={e => this.handleDefaultFieldChange('orders', e)}
                                        value={this.state.orders.text}
                                    >
                                        {
                                            ['AT', 'MOB', 'IDT', 'PCS'].map(i =>
                                                <Option key={i}>{i}</Option>
                                            )
                                        }
                                    </Select>
                                </span>
                            </Col>
                        </FormItem>
                        <FormItem validateStatus={this.state.promo_awards_eval.validateStatus}
                                  help={this.state.promo_awards_eval.errorMsg} className="poll-form-row">
                            <Col span={24}>
                                Promotions, Awards or Evaluations*
                            </Col>
                            <Col span={24}>
                                <span style = {{ marginRight: '18px' }}>
                                    <Select
                                        onChange={e => this.handleDefaultFieldChange('promo_awards_eval', e)}
                                        value={this.state.promo_awards_eval.text}
                                    >
                                        {
                                            ['Professional Military Education', 'Physical Training/Testing', 'Training'].map(i =>
                                                <Option key={i}>{i}</Option>
                                            )
                                        }
                                    </Select>
                                </span>
                            </Col>
                        </FormItem>
                        <FormItem validateStatus={this.state.logistics.validateStatus}
                                  help={this.state.logistics.errorMsg} className="poll-form-row">
                            <Col span={24}>
                                Logistics*
                            </Col>
                            <Col span={24}>
                                <span style = {{ marginRight: '18px' }}>
                                    <Select
                                        onChange={e => this.handleDefaultFieldChange('logistics', e)}
                                        value={this.state.logistics.text}
                                    >
                                        {
                                            [
                                                'Pay',
                                                'Family Programs',
                                                'Defense Travel Services',
                                                'Defense Enrollment Eligibility Reporting System',
                                                'Equipment',
                                                'PHA/Dental/Contract'
                                            ].map(i =>
                                                <Option key={i}>{i}</Option>
                                            )
                                        }
                                    </Select>
                                </span>
                            </Col>
                        </FormItem>
                        <FormItem validateStatus={this.state.services.validateStatus}
                                  help={this.state.services.errorMsg} className="poll-form-row">
                            <Col span={24}>
                                Services*
                            </Col>
                            <Col span={24}>
                                <Input
                                    style = {{fontSize: '16px'}}
                                    autosize={{minRows: 3, maxRows: 6}}
                                    value = {this.state.services.text}
                                    onChange={e => this.handleDefaultFieldChange('services', e.target.value)}
                                />
                            </Col>
                        </FormItem>
                        <FormItem validateStatus={this.state.system_website_access.validateStatus}
                                  help={this.state.system_website_access.errorMsg} className="poll-form-row">
                            <Col span={24}>
                                USAR System/Website Access*
                            </Col>
                            <Col span={24}>
                                <Input
                                    style = {{fontSize: '16px'}}
                                    autosize={{minRows: 3, maxRows: 6}}
                                    value = {this.state.system_website_access.text}
                                    onChange={e => this.handleDefaultFieldChange('system_website_access', e.target.value)}
                                />
                            </Col>
                        </FormItem>
                        <FormItem className="poll-form-row">
                            <Col span={24}>
                                Please elaborate your suggestion
                            </Col>
                            <Col span={24}>
                                <TextArea
                                    style = {{fontSize: '16px'}}
                                    autosize={{minRows: 3, maxRows: 6}}
                                    value = {this.state.other.text}
                                    onChange={e => this.handleDefaultFieldChange('other', e.target.value)}
                                />
                            </Col>
                        </FormItem>
                        <FormItem className="poll-form-row">
                            <Button type="primary"
                                htmlType="submit"
                                size="large"
                                disabled={this.isFormInvalid()}
                                className="create-poll-form-button">Submit</Button>
                        </FormItem>
                    </Form>
                </div>
            </div>
        );
    }
}

function PollChoice(props) {
    return (
        <FormItem validateStatus={props.choice.validateStatus}
        help={props.choice.errorMsg} className="poll-form-row">
            <Input
                placeholder = {'Choice ' + (props.choiceNumber + 1)}
                size="large"
                value={props.choice.text}
                className={ props.choiceNumber > 1 ? "optional-choice": null}
                onChange={(event) => props.handleChoiceChange(event, props.choiceNumber)} />

            {
                props.choiceNumber > 1 ? (
                <Icon
                    className="dynamic-delete-button"
                    type="close"
                    disabled={props.choiceNumber <= 1}
                    onClick={() => props.removeChoice(props.choiceNumber)}
                /> ): null
            }
        </FormItem>
    );
}


export default NewPoll;
